from django.contrib import admin
from .models import *
from django.contrib.auth.models import Group, User

admin.site.unregister(Group)
admin.site.unregister(User)

admin.site.register(Availability)
admin.site.register(Region)
