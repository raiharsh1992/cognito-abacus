from django.shortcuts import render
from django.http import HttpResponseRedirect

def redirect(request):
    return HttpResponseRedirect("/dashboard/")

def dashboard(request):
    context_data = {}
    return render(request, 'dashboard.html', context_data)
