from django.db import models

class Availability(models.Model):
    availability = models.CharField(max_length=300, help_text="Availability Zone name")

    def __str__(self):
        return self.availability

class Region(models.Model):
    region_name = models.CharField(max_length=300, help_text="Region Name")
    zone = models.ManyToManyField(Availability)

    def __str__(self):
        self.region_name
