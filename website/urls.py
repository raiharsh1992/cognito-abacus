from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.redirect, name="redirect"),
    url(r'^dashboard/$', views.dashboard, name="dashboard"),
]
